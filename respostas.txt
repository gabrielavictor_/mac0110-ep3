/***** MAC0110 - EP3 *****/
  Nome: <Gabriela Victor>
  NUSP: <11795381>

/***** Parte 1 - Entendendo o código *****/

    1) Qual a probabilidade de um elemento da matriz ser grama (ou terreno)? Você pode expressar essa possibilidade utilizando um valor numérico ou uma fórmula.

        A probabilidade de um elemento da matriz ser grama é de O.64.

    2) Analise o código e diga: qual a diferença entre o terreno e o terreno especial?

        Enquanto o terreno tem probabilidade igual a 0.64 de ser um elemento da ilha, o terreno especial tem essa mesma probabilidade igual a 0.01. Portanto, o terreno especial é bem mais raro que o terreno nessa simulação de ilha.

    3) Dados os valores iniciais das constantes, qual a energia necessária para um lobo se reproduzir? E um coelho?

        Os valores iniciais das constantes estabelecem que a energia do lobo é igual a 10, a do coelho é igual a 6 e o fator de reprodução é 2. Analisando o código da função processa_animal, percebe-se que o limiar de energia a ser atingido para que a reprodução ocorra corresponde à multiplicação entre o fator de reprodução e a energia do animal, portanto, considerando-se as energias iniciais das constantes, o coelho necessita atingir o valor 12 de energia (2*6) e o lobo precisa de 20 (2*10).

/***** Parte 2 - Completando as funções *****/

    4) Ao contrário das funções ocupa_vizinho! e reproduz!, a função morre! não recebe a matriz de energia como parâmetro. Por quê não é necessário alterar a energia com a morte de um animal?

        As funções ocupa_vizinho! e reproduz! ambas simulam situações em que a energia de dois animais é alterada, seja pelo consumo de um animal por outro, seja pela bipartição de um animal e a subsequente divisão de energia ocorrida entre animal pai e animal filho. No entanto, a função morre! não simula uma alteração de energia: ela somente remove o animal cujo nível de energia chegou a zero e o substitui por terreno. O esgotamento da energia é anterior a morre!, sendo essa função uma consequência de outras funções responsáveis pelo processamento de energia nessa simulação de ilha. Portanto, não é necessária a alteração da matriz energia na função morre! porque ela não altera algum valor de energia. 


/***** Parte 3 - Teste e simulação *****/

    5) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece na ilha?

        O número de coelhos diminui drasticamente, chegando a zero conforme o aumento de iterações. Restam apenas lobos e comida.

    6) Qual combinação de constantes leva a ilha a ser dominada por coelhos? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

        Para que a ilha seja dominada por coelhos, basta alterar a constante PROBABILIDADE_LOBO para 0.0.

    7) Qual combinação de constantes leva a ilha não ter nenhum animal? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

        Para a ilha não ter nenhum animal, pode-se alterar as constantes PROBABILIDADE_LOBO e PROBABILIDADE_COELHO para 0.0 ou para um número bem pequeno como 0.0001 (nesse caso, mantendo TAMANHO_ILHA < 100). 

/***** Parte 3 - Usando DataFrames e plotando gráficos *****/

    8) Qual a diferença entre a função simula - da parte 3 - e a função simula2?
     
       Existe mais de uma diferença: Enquanto a função simula recebe apenas um parâmetro (iterações), a função simula2 recebe dois (iterações e imprime); a função simula sempre processa e imprime a ilha, mas a simula2 pode processar e imprimir ou apenas processar (consequência da diferença anterior); a função simula2 utiliza DataFrames, a função simula não; a função simula2 utiliza uma nova função, a atualiza!, diferentemente da simula; simula2 retorna o dataframe "simulacao" enquanto a simula não tem nada após return.


    9) A função gera_graficos possui sintaxes diferentes das vistas em aula e nos outros exercícios. Apesar disso, é possível entender o que a função faz, sem rodá-la e sem conhecer detalhes sobre os pacotes de gráficos. Sem rodar a função, responda: quantos gráficos a função plota? Qual o conteúdo de cada gráfico?

        A função gera_graficos gera dois gráficos: um com o número de lobos e de coelhos na ilha; outro com a soma de todas as energias da ilha(resultado da função energia_total) e com o número de comidas na ilha.

    10) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu na parte 3?

        Os gráficos mostrados são semelhantes ao respondido na parte 3: o primeiro mostra a linha dos coelhos dimiuindo até zero e a dos lobos crescendo, diminuindo e se estabilizando; o segundo mostra a energia total e a comida diminuindo, mas depois de estabilizando.
 
    11) Usando a combinação de constantes da questão 6 - que leva a ilha a ser dominada por coelhos - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?

       Quando a constante PROBABILIDADE_LOBO é zerada, o gráfico COELHOS e LOBOS apresenta a linha dos lobos constante no zero e a linha dos coelhos apresenta uma curva: estável por pouco tempo, cresce, atinge um pico e depois diminui. Com o gráfico é mais fácil notar a variação de crescimento dos coelhos na ilha. O observado é similar ao relatado na parte 3.
        Já o gráfico ENERGIA TOTAL e COMIDA apresenta o decrescimento de ambas as variáveis e a estabilização das mesmas após a iteração 20.
        
    12) Usando a combinação de constantes da questão 7 - que leva a ilha à extinção de todos os animais - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?
        
       Com as constantes PROBABILIDADE_LOBO e PROBABILIDADE_COELHO zeradas, o gráfico COELHOS e LOBOS apresenta duas linhas sobrepostas indicando o valor constante zero o que condiz com o respondido na parte 3. Já o segundo gráfico de ENERGIA TOTAL e COMIDA apresenta o crescimento da energia total da ilha e da quantidade de comida conforme seguem as iterações, o que é esperado numa simulação de ilha sem animais para consumir a comida e ocupar o espaço dela.


       
